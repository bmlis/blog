class CommentsController < ApplicationController

  before_filter :authenticate_user!
  
  def create
    @article = Article.find(params[:article_id])
    @comment = Comment.new(comment_params.merge article: @article)
    @comment.user = current_user
    
    @comment.save 
    redirect_to article_path(@article)
  end

  def destroy
    @article = Article.find(params[:article_id])
    @comment = @article.comments.find(params[:id])
    if current_user.owner?(@comment)
      @comment.destroy
    end
    redirect_to article_path(@article)
  end
 
  private
    def comment_params
      params.require(:comment).permit(:body)
    end

end
